import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  data = null;
  @Output() dataChanged = new EventEmitter();

  constructor() { }

  setData(data) {
    console.log(data);
    this.data = data;
    this.dataChanged.emit(data);
  }
}
