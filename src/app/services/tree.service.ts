import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TreeService {

  baseUrl = 'http://localhost:8000'
  
  constructor(private _httpClient: HttpClient) { }

  getAllTrees() {
    const url = `${this.baseUrl}/api/node-trees`;
    return this._httpClient.get(url);
  }

  putNode(id, title, parent=null) {
    const url = `${this.baseUrl}/api/node-trees/${id}`;
    return this._httpClient.put(url, {title: title, parent: parent});
  }

  postNode(title, parent=null) {
    const url = `${this.baseUrl}/api/node-trees`;
    return this._httpClient.post(url, {title: title, parent: parent});
  }

  deleteNode(id) {
    const url = `${this.baseUrl}/api/node-trees/${id}`;
    return this._httpClient.delete(url);
  }
}
