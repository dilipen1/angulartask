import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeService } from 'src/app/services/tree.service';
import { SharedService } from 'src/app/services/shared.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-tree-wrapper',
  templateUrl: './tree-wrapper.component.html',
  styleUrls: ['./tree-wrapper.component.scss']
})
export class TreeWrapperComponent implements OnInit, OnDestroy {

  trees: any = [];

  newForm = {
    title: '',
    parent: null,
  }

  editForm = {
    id: null,
    title: null,
    parent: null,
  }

  private _sharedServiceSubscription = null;

  constructor(private _treeService: TreeService, private _sharedService: SharedService,
    private _toasterService: ToastrService) { }

  ngOnInit() {
    this.loadTrees();
    this._sharedServiceSubscription = this._sharedService.dataChanged.subscribe(
      data => {
        switch(data['action']) {
          case 'SHOW_TREE_EDIT_FORM':
            console.log(data);
            console.log(this.trees);
            this.editForm.id = data.tree.id;
            this.editForm.title = data.tree.title;
            this.editForm.parent = data.parent;
            break;
          case 'CLEAR_TREE_EDIT_FORM':
            this.onCancel();
            break;
        }
      }
    );
  }

  loadTrees() {
    this._treeService.getAllTrees().subscribe(
      data => {
        this.trees = data;
      }, error => { 
        this._toasterService.error(error.message, 'Error');
        console.error('oops', error.message);
      }
    );
  }

  onCancel() {
    this.editForm = {
      id: null,
      title: null,
      parent: null,
    };
  }

  onDeleteNodeTree() {
    if (window.confirm('Are you sure to delete ?') === false) {
      return;
    }
    this._treeService.deleteNode(this.editForm.id).subscribe(
      data => {
        this._sharedService.setData({
          action: 'DELETE_TREE',
          tree: {id: this.editForm.id}
        });
        this.editForm = {
          id: null,
          title: null,
          parent: null,
        };
      },
      error => {
        this._toasterService.error(error.message, 'Error');
        console.error('oops', error.message);
      }
    );
  }

  onPostNodeTree(parent = null) {
    this._treeService.postNode(this.newForm.title, parent).subscribe(
      data => {
        this._sharedService.setData({
          action: 'ADD_TREE',
          tree: {id: data['id'], title: data['title']},
          parent: this.editForm.id
        });
        this.newForm.title = '';
        this.newForm.parent = null;
      },
      error => {
        this._toasterService.error(error.message, 'Error');
        console.error('oops', error.message);
      }
    );
  }

  onPutNodeTree() {
    this._treeService.putNode(this.editForm.id, this.editForm.title, this.editForm.parent).subscribe(
      data => {
        this._sharedService.setData({
          action: 'UPDATE_TREE',
          tree: {id: data['id'], title: data['title']},
          parent: this.editForm.parent
        });
        this.editForm = {
          id: null,
          title: null,
          parent: null,
        };
      },
      error => {
        this._toasterService.error(error.message, 'Error');
        console.error('oops', error.message);
      }
    );
  }

  ngOnDestroy() {
    this._sharedServiceSubscription.unsubscribe();
  }

}
