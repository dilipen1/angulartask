import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TreeService } from 'src/app/services/tree.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss']
})
export class TreeViewComponent implements OnInit, OnDestroy {
  
  @Input()
  trees;

  @Input()
  parent = null;

  form = {
    id: null,
    title: null,
  }

  private _sharedServiceSubscription = null;

  constructor(private _treeService: TreeService, private _sharedService: SharedService) { }

  ngOnInit() {
    this._sharedServiceSubscription = this._sharedService.dataChanged.subscribe(
      data => {
        switch(data['action']) {
          case 'ADD_TREE':
            console.log(data['parent']);
            console.log(this.parent);
            if (data.parent === this.parent) {
              this.trees.push(data['tree']);
            }
            break;
            case 'UPDATE_TREE':
                console.log(data);
                console.log(this.parent);
                console.log(this.trees);
                const len = this.trees.length;
                for(let i=0;i<len;i++) {
                  if (!!this.trees[i] && this.trees[i]['id'] == data['tree']['id']) {
                    this.trees[i]['title'] = data['tree']['title'];
                  }
                }
                break;
            case 'DELETE_TREE':
                console.log(data);
                console.log(this.parent);
                console.log(this.trees);
                if(!this.trees) {
                  return;
                }
                const len1 = this.trees.length;
                for(let i=0;i<len1;i++) {
                  if (!!this.trees[i] && this.trees[i]['id'] == data['tree']['id']) {
                    this.trees.splice(i,1);
                  }
                }
                break;
        }
      }
    );
  }

  onEditNodeTree(tree) {
    this._sharedService.setData({
      action: 'SHOW_TREE_EDIT_FORM',
      tree: tree,
      parent: this.parent,
    });
  }

  onClearEditNodeTree() {
    this._sharedService.setData({
      action: 'CLEAR_TREE_EDIT_FORM',
    });
  }

  onToggleTreeView(tree) {
    tree.expand = !tree.expand;
    if (tree.expand) {
      this.onEditNodeTree(tree);
    } else {
      this.onClearEditNodeTree();
    }
  }

  ngOnDestroy() {
    this._sharedServiceSubscription.unsubscribe();
  }
}
