import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TreeWrapperComponent } from './components/tree-wrapper/tree-wrapper.component';


const routes: Routes = [
    {path: '', component: TreeWrapperComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
